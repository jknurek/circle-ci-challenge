const express = require('express')
const app = express()
const path = require('path')
const port = (process.env.PORT) ? process.env.PORT : 3000
const htmlFile = path.join(__dirname + '/index.html')

app.get('/', (req, res) => res.sendFile(htmlFile))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
