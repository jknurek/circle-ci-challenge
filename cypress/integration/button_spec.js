describe('Simple button test', function() {
  it('change text', function() {
    cy.visit('http://localhost:3000')

    cy.get('#text').should('have.text', 'hello world')

    cy.get('#changeText').click()

    cy.get('#text').should('have.text', 'new text')

  })
})